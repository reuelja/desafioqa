*** Settings ***
Resource                CommonsResource.robot
Test Setup              Abrir navegador
Test Teardown           Close All Browsers

*** Test Case ***
Cenário 01: Validar Pesquisa medica por área e especialidade
    Dado que eu acesse o Guia Médico
    Quando eu realizar a pesquisa por medicos em uma cidade e especialidade especifica      Rio de Janeiro   Rio de janeiro         Alergia e Imunologia
    Então deve exibir apenas medicos com sua especialidade na cidade especificada           Rio de Janeiro   Alergia e Imunologia

Cenário 02: Validar Pesquisa medica por área exceto uma em especifico
    Dado que eu acesse o Guia Médico
    Quando eu realizar a pesquisa por medicos em uma cidade e especialidade especifica      Rio de Janeiro   Rio de janeiro         Alergia e Imunologia
    Então não deve deve exibir cidade especificada  São Paulo

