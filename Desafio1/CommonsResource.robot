*** Settings ***
Library             SeleniumLibrary
Library             String
Library             BuiltIn
Library             Collections


*** Variable ***
${BROWSER_CHROME}       chrome
${OPTIONS}              add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
${URL}                  https://www.unimed.coop.br/ 

#elementos da pagina
${GUIA_MEDICO}              xpath=//a[@href="/guia-medico"]/h4
${BUSCA_DETALHADA}          id=busca_detalhada
${INPUT_BOX_ESTADO}         id=react-select-2-input
${INPUT_BOX_CIDADE}         id=react-select-3-input
${ID_RIO_DE_JANEIRO_EST}    id=react-select-2-option-18
${ID_RIO_DE_JANEIRO_EST2}   id=react-select-4-option-18
${ID_RIO_DE_JANEIRO_CID}    id=react-select-3-option-67
${ID_RIO_DE_JANEIRO_CID2}   id=react-select-5-option-67
${RADIO_UNIMED_RIO}         xpath=//form[@class="form-escolher-unimed-gm"]/label/div/input
${BUTTON_CONTINUAR_PESQ}    xpath=//div[@class="margin-top margin-bottom"]/button[2]
${INPUT_BOX_ESTADO2}        id=react-select-4-input
${INPUT_BOX_CIDADE2}        id=react-select-5-input
${BUTTON_PESQUISAR}         id=btn_pesquisar
${RESULTADO_ENCONTRADO}     id=txt_resultado_encontrado
${INPUT_ESPECIALIDADE}      xpath=//div[@id="campo_especialidade"]/div/div/div/div[2]/div/input
${OPTION_ESPECIALIDADE}     id=react-select-8-option-0
${1-LINHAPESQUISA}          xpath=//div[@class="fade-in"]/div/div[1]/div/div[2]/div/span/p
${PAGINA_2}                 xpath=//div[@class="span12 pagination text-center no-margin-left"]/ul //*[contains(text(),'2')]
${PAGINA_3}                 xpath=//div[@class="span12 pagination text-center no-margin-left"]/ul //*[contains(text(),'3')]

*** Keywords ***
#BDD Area
Dado que eu acesse o Guia Médico
    Wait Until Element Is Visible       ${GUIA_MEDICO}
    Click Element                       ${GUIA_MEDICO} 

Quando eu realizar a pesquisa por medicos em uma cidade e especialidade especifica
    [Arguments]                         ${ESTADO}                       ${CIDADE}   ${ESPECIALIDADE_ESP}
    Click Element                       ${BUSCA_DETALHADA}
    Wait Until Element Is Visible       ${INPUT_BOX_ESTADO}
    Input Text                          ${INPUT_BOX_ESTADO}             ${ESTADO}
    Wait Until Element Is Visible       ${ID_RIO_DE_JANEIRO_EST}
    Click Element                       ${ID_RIO_DE_JANEIRO_EST}
    Input Text                          ${INPUT_BOX_CIDADE}             ${CIDADE}
    Wait Until Element Is Visible       ${ID_RIO_DE_JANEIRO_CID}
    Click Element                       ${ID_RIO_DE_JANEIRO_CID}
    Wait Until Element Is Visible       ${RADIO_UNIMED_RIO}
    Click Element                       ${RADIO_UNIMED_RIO}
    Click Element                       ${BUTTON_CONTINUAR_PESQ}

    Input Text                          ${INPUT_BOX_ESTADO2}            ${ESTADO}
    Wait Until Element Is Visible       ${ID_RIO_DE_JANEIRO_EST2}
    Click Element                       ${ID_RIO_DE_JANEIRO_EST2}
    Input Text                          ${INPUT_BOX_CIDADE2}            ${CIDADE}
    Wait Until Element Is Visible       ${ID_RIO_DE_JANEIRO_CID2}
    Click Element                       ${ID_RIO_DE_JANEIRO_CID2}
    Wait Until Element Is Visible       ${INPUT_ESPECIALIDADE}
    Input Text                          ${INPUT_ESPECIALIDADE}          ${ESPECIALIDADE_ESP}
    Click Element                       ${OPTION_ESPECIALIDADE}
    Click Element                       ${BUTTON_PESQUISAR}
    Wait Until Element Is Visible       ${RESULTADO_ENCONTRADO}
    Wait Until Element Is Visible       ${1-LINHAPESQUISA}

Então deve exibir apenas medicos com sua especialidade na cidade especificada
    [Arguments]                                          ${CIDADE_ESPECIFICADA}         ${ESPECIALIDADE}    
    Lista de Endereços
    Lista de Especialidades
    Validando lista conforme especificação               ${CIDADE_ESPECIFICADA}         @{Lista_De_Endereços}
    Validando lista conforme especificação               ${ESPECIALIDADE}               @{Lista_De_Especialidades}

Então não deve deve exibir cidade especificada
    [Arguments]                                          ${CIDADE_ESPECIFICADA}
    Lista de Endereços
    Validando lista conforme exclulsão especificada      ${CIDADE_ESPECIFICADA}          @{Lista_De_Endereços}
    Click Element                                        ${PAGINA_2}
    Wait Until Element Is Visible                        ${1-LINHAPESQUISA}
    Validando lista conforme exclulsão especificada      ${CIDADE_ESPECIFICADA}          @{Lista_De_Endereços}
    Sleep   2                                            #Melhorar isso
    Click Element                                        ${PAGINA_3}
    Wait Until Element Is Visible                        ${1-LINHAPESQUISA}
    Validando lista conforme exclulsão especificada      ${CIDADE_ESPECIFICADA}          @{Lista_De_Endereços}
#BDD Area


Abrir navegador
    Open Browser        ${URL}      ${BROWSER_CHROME}   
    Maximize Browser Window

Lista de Endereços
    ${ENDERECO_01}          Get Text        //div[@class="fade-in"]/div/div[1]/div/div[2]/div/span/p
    ${ENDERECO_02}          Get Text        //div[@class="fade-in"]/div/div[2]/div/div[2]/div/span/p
    ${ENDERECO_03}          Get Text        //div[@class="fade-in"]/div/div[3]/div/div[2]/div/span/p
    ${ENDERECO_04}          Get Text        //div[@class="fade-in"]/div/div[4]/div/div[2]/div/span/p
    ${ENDERECO_05}          Get Text        //div[@class="fade-in"]/div/div[5]/div/div[2]/div/span/p
    ${ENDERECO_06}          Get Text        //div[@class="fade-in"]/div/div[6]/div/div[2]/div/span/p
    ${ENDERECO_07}          Get Text        //div[@class="fade-in"]/div/div[7]/div/div[2]/div/span/p
    ${ENDERECO_08}          Get Text        //div[@class="fade-in"]/div/div[8]/div/div[2]/div/span/p
    ${ENDERECO_09}          Get Text        //div[@class="fade-in"]/div/div[9]/div/div[2]/div/span/p
    ${ENDERECO_10}          Get Text        //div[@class="fade-in"]/div/div[10]/div/div[2]/div/span/p
    ${ENDERECO_11}          Get Text        //div[@class="fade-in"]/div/div[11]/div/div[2]/div/span/p
    ${ENDERECO_12}          Get Text        //div[@class="fade-in"]/div/div[12]/div/div[2]/div/span/p
    ${ENDERECO_13}          Get Text        //div[@class="fade-in"]/div/div[13]/div/div[2]/div/span/p
    ${ENDERECO_14}          Get Text        //div[@class="fade-in"]/div/div[14]/div/div[2]/div/span/p
    ${ENDERECO_15}          Get Text        //div[@class="fade-in"]/div/div[15]/div/div[2]/div/span/p
    ${ENDERECO_16}          Get Text        //div[@class="fade-in"]/div/div[16]/div/div[2]/div/span/p
    ${ENDERECO_17}          Get Text        //div[@class="fade-in"]/div/div[17]/div/div[2]/div/span/p
    ${ENDERECO_18}          Get Text        //div[@class="fade-in"]/div/div[18]/div/div[2]/div/span/p
    ${ENDERECO_19}          Get Text        //div[@class="fade-in"]/div/div[19]/div/div[2]/div/span/p
    ${ENDERECO_20}          Get Text        //div[@class="fade-in"]/div/div[20]/div/div[2]/div/span/p

    @{Lista_De_Endereços}   Create List  ${ENDERECO_01}  ${ENDERECO_02}  ${ENDERECO_03}  ${ENDERECO_04}  
    ...  ${ENDERECO_05}  ${ENDERECO_06}  ${ENDERECO_07}  ${ENDERECO_08}  ${ENDERECO_09}  ${ENDERECO_10}  
    ...  ${ENDERECO_11}  ${ENDERECO_12}  ${ENDERECO_13}  ${ENDERECO_14}  ${ENDERECO_15}  ${ENDERECO_16}  
    ...  ${ENDERECO_17}  ${ENDERECO_18}  ${ENDERECO_19}  ${ENDERECO_20}  

    Set Test Variable  @{Lista_De_Endereços}
    Log Many  @{Lista_De_Endereços}

Lista de Especialidades
    ${ESPECIALIDADE_01}          Get Text        //div[@class="fade-in"]/div/div[1]/div/div[2]/div/p
    ${ESPECIALIDADE_02}          Get Text        //div[@class="fade-in"]/div/div[2]/div/div[2]/div/p
    ${ESPECIALIDADE_03}          Get Text        //div[@class="fade-in"]/div/div[3]/div/div[2]/div/p
    ${ESPECIALIDADE_04}          Get Text        //div[@class="fade-in"]/div/div[4]/div/div[2]/div/p
    ${ESPECIALIDADE_05}          Get Text        //div[@class="fade-in"]/div/div[5]/div/div[2]/div/p
    ${ESPECIALIDADE_06}          Get Text        //div[@class="fade-in"]/div/div[6]/div/div[2]/div/p
    ${ESPECIALIDADE_07}          Get Text        //div[@class="fade-in"]/div/div[7]/div/div[2]/div/p
    ${ESPECIALIDADE_08}          Get Text        //div[@class="fade-in"]/div/div[8]/div/div[2]/div/p
    ${ESPECIALIDADE_09}          Get Text        //div[@class="fade-in"]/div/div[9]/div/div[2]/div/p
    ${ESPECIALIDADE_10}          Get Text        //div[@class="fade-in"]/div/div[10]/div/div[2]/div/p
    ${ESPECIALIDADE_11}          Get Text        //div[@class="fade-in"]/div/div[11]/div/div[2]/div/p
    ${ESPECIALIDADE_12}          Get Text        //div[@class="fade-in"]/div/div[12]/div/div[2]/div/p
    ${ESPECIALIDADE_13}          Get Text        //div[@class="fade-in"]/div/div[13]/div/div[2]/div/p
    ${ESPECIALIDADE_14}          Get Text        //div[@class="fade-in"]/div/div[14]/div/div[2]/div/p
    ${ESPECIALIDADE_15}          Get Text        //div[@class="fade-in"]/div/div[15]/div/div[2]/div/p
    ${ESPECIALIDADE_16}          Get Text        //div[@class="fade-in"]/div/div[16]/div/div[2]/div/p
    ${ESPECIALIDADE_17}          Get Text        //div[@class="fade-in"]/div/div[17]/div/div[2]/div/p
    ${ESPECIALIDADE_18}          Get Text        //div[@class="fade-in"]/div/div[18]/div/div[2]/div/p
    ${ESPECIALIDADE_19}          Get Text        //div[@class="fade-in"]/div/div[19]/div/div[2]/div/p
    ${ESPECIALIDADE_20}          Get Text        //div[@class="fade-in"]/div/div[20]/div/div[2]/div/p

	@{Lista_De_Especialidades}   Create List  ${ESPECIALIDADE_01}  ${ESPECIALIDADE_02}  ${ESPECIALIDADE_03}  ${ESPECIALIDADE_04}  
    ...  ${ESPECIALIDADE_05}  ${ESPECIALIDADE_06}  ${ESPECIALIDADE_07}  ${ESPECIALIDADE_08}  ${ESPECIALIDADE_09}  ${ESPECIALIDADE_10}  
    ...  ${ESPECIALIDADE_11}  ${ESPECIALIDADE_12}  ${ESPECIALIDADE_13}  ${ESPECIALIDADE_14}  ${ESPECIALIDADE_15}  ${ESPECIALIDADE_16}  
    ...  ${ESPECIALIDADE_17}  ${ESPECIALIDADE_18}  ${ESPECIALIDADE_19}  ${ESPECIALIDADE_20}  

    Set Test Variable  @{Lista_De_Especialidades}
    Log Many  @{Lista_De_Especialidades}

Validando lista conforme especificação
    [Arguments]     ${Item_Verificador}       @{LISTA}
    :FOR            ${ITEM}   IN  @{LISTA}
    \               Should Contain  ${ITEM}   ${Item_Verificador}

Validando lista conforme exclulsão especificada
    [Arguments]     ${Item_Verificador}       @{LISTA}
    :FOR            ${ITEM}   IN  @{LISTA}
    \               Should Not Contain  ${ITEM}   ${Item_Verificador}