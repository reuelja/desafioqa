const assert = require('chai').assert

const filme = [
    {idFilme:1, nomeFilme:'Senhor dos Aneis', Genero:'Fantadsia', RS: 45.0},
    {idFilme:2, nomeFilme:'As Branquelas', Genero:'Comédia', RS: 55.0},
    {idFilme:3, nomeFilme:'Velozes e Furiosos 7', Genero:'Ação', RS: 100.0},
    {idFilme:4, nomeFilme:'Velozes e Furiosos 6', Genero:'Ação', RS: 55.0},
    {idFilme:5, nomeFilme:'Thje Scapegoat', Genero:'Drama', RS: 100.0},
    {idFilme:6, nomeFilme:'Meu Malvado Favorito', Genero:'Animação', RS: 200.0}
]

const programDesconto = (valor) => {
    if(valor > 100 && valor <= 200){
        return valor - (valor * 0.1)
    }else if(valor > 200 && valor <= 300){
        return valor - (valor * 0.2)
    }else if(valor > 300 && valor <= 400){
        return valor - (valor * 0.25)
    }else if(valor > 400){
        return valor - (valor * 0.3)
    }
    return valor
}

const programAcao = (genero, valor) => {
    if (genero == 'Ação'){
        return valor - (valor * 0.05)
    }
    return valor
}

const filmeIdentificado = (id) => {
    return filme.filter(x => x.idFilme == id)
}

const FilmeComDeconto = (id) => {
    return Number.parseInt(filmeIdentificado(id).map(x => programAcao(x.Genero, x.RS))) 
}

const carrinhoDeFilmes = (...valorFilme) =>{
   let soma = (total, valorFilme) => total + valorFilme
   return valorFilme.reduce(soma, 0)
}


// console.log('CARRINHO: ', carrinhoDeFilmes(2, 3, 5))



describe('Testando funcionalidade de desconto', () =>{

    it('Se o valor da compra for de R$ 100,00 ou abaixa, o valor se mantem', (done) => {
        let filme1 = FilmeComDeconto(1)
        let filme2 = FilmeComDeconto(2)
        let valorCarrinho = carrinhoDeFilmes(filme1, filme2)
        let valorComDesconto = programDesconto(valorCarrinho)
        assert.equal(valorCarrinho, valorComDesconto)
        done()
    });

    it('Se o valor da compra for acima de R$ 100,00 e igual ou menor que R$ 200,00 terá 10% de desconto', (done) => {
        let filme1 = FilmeComDeconto(1)
        let filme2 = FilmeComDeconto(5)
        let valorCarrinho = carrinhoDeFilmes(filme1, filme2)
        let valorComDesconto = programDesconto(valorCarrinho)
        let valorcalculado = valorCarrinho - (valorCarrinho * 0.1)
        assert.equal(valorcalculado, valorComDesconto)
        done()
    });

    it('Se o valor da compra for acima de R$ 200,00 e igual ou menor que R$ 300,00 terá 20% de desconto', (done) => {
        let filme1 = FilmeComDeconto(1)
        let filme2 = FilmeComDeconto(6)
        let valorCarrinho = carrinhoDeFilmes(filme1, filme2)
        let valorComDesconto = programDesconto(valorCarrinho)
        let valorcalculado = valorCarrinho - (valorCarrinho * 0.2)
        assert.equal(valorcalculado, valorComDesconto)
        done()
    });

    it('Se o valor da compra for acima de R$ 300,00 e igual ou menor que R$ 400,00 terá 25% de desconto', (done) => {
        let filme1 = FilmeComDeconto(1)
        let filme2 = FilmeComDeconto(6)
        let filme3 = FilmeComDeconto(5)
        let valorCarrinho = carrinhoDeFilmes(filme1, filme2, filme3)
        let valorComDesconto = programDesconto(valorCarrinho)
        let valorcalculado = valorCarrinho - (valorCarrinho * 0.25)
        assert.equal(valorcalculado, valorComDesconto)
        done()
    });

    it('Se o valor da compra for acima de R$ 400,00 terá 30% de desconto', (done) => {
        let filme1 = FilmeComDeconto(5)
        let filme2 = FilmeComDeconto(6)
        let filme3 = FilmeComDeconto(6)
        let valorCarrinho = carrinhoDeFilmes(filme1, filme2, filme3)
        let valorComDesconto = programDesconto(valorCarrinho)
        let valorcalculado = valorCarrinho - (valorCarrinho * 0.3)
        assert.equal(valorcalculado, valorComDesconto)
        done()
    });

    it('Se o valor da compra for abaixo de R$ 100,00 porem o filme for de ação, terá desconto de 5%', (done) => {
        let filme1 = FilmeComDeconto(3)
        let valorCarrinho = carrinhoDeFilmes(filme1)
        let valorComDesconto = programDesconto(valorCarrinho)
        assert.equal(filme1, valorComDesconto)
        done()
    });

    it('Fazendo somatorio de descontos, com filmes comuns e com os de ação, com exemplo de 10% de desconto', (done) => {
        let filme1 = FilmeComDeconto(5)
        let filme4 = FilmeComDeconto(4)
        let valorCarrinho = carrinhoDeFilmes(filme1,filme4)
        let valorComDesconto = programDesconto(valorCarrinho)
        let valorcalculado = valorCarrinho - (valorCarrinho * 0.1)
        assert.equal(valorcalculado, valorComDesconto)
        done()
    });

    it('Fazendo somatorio de descontos, com filmes comuns e com os de ação, com exemplo de 20% de desconto', (done) => {
        let filme1 = FilmeComDeconto(5)
        let filme4 = FilmeComDeconto(6)
        let valorCarrinho = carrinhoDeFilmes(filme1,filme4)
        let valorComDesconto = programDesconto(valorCarrinho)
        let valorcalculado = valorCarrinho - (valorCarrinho * 0.2)
        assert.equal(valorcalculado, valorComDesconto)
        done()
    });

    it('Fazendo somatorio de descontos, com filmes comuns e com os de ação, com exemplo de 25% de desconto', (done) => {
        let filme1 = FilmeComDeconto(3)
        let filme2 = FilmeComDeconto(5)
        let filme3 = FilmeComDeconto(6)
        let valorCarrinho = carrinhoDeFilmes(filme1, filme2, filme3)
        let valorComDesconto = programDesconto(valorCarrinho)
        let valorcalculado = valorCarrinho - (valorCarrinho * 0.25)
        assert.equal(valorcalculado, valorComDesconto)
        done()
    });

    it('Fazendo somatorio de descontos, com filmes comuns e com os de ação, com exemplo de 30% de desconto', (done) => {
        let filme1 = FilmeComDeconto(1)
        let filme2 = FilmeComDeconto(2)
        let filme3 = FilmeComDeconto(3)
        let filme4 = FilmeComDeconto(4)
        let filme5 = FilmeComDeconto(5)
        let filme6 = FilmeComDeconto(6)
        let valorCarrinho = carrinhoDeFilmes(filme1, filme2, filme3, filme4, filme5, filme6)
        let valorComDesconto = programDesconto(valorCarrinho)
        let valorcalculado = valorCarrinho - (valorCarrinho * 0.3)
        assert.equal(valorcalculado, valorComDesconto)
        done()
    });

});