*** Settings ***
Library             RequestsLibrary
Library             Collections
Library             JSONLibrary
Library             String

*** Variable ***
${ID_FILME}=                tt1285016
${ID_FILME_INEXISTENTE}=    tt9995016
${API_KEY}=                 52ec71bf
${URL}=                     http://www.omdbapi.com/
&{HEADER}=                  Content-Type=application/json
&{CONFERENCIA}=             Title=The Social Network   Year=2010  Language=English, French
&{FILME_INEXISTENTE}=       Response=False   Error=Error getting data.

*** Keywords ***
Conectar API
    Create Session      API     ${URL}   headers=${HEADER}
    log                 ${URL}

Requisitar URL
    [Arguments]         ${IDFILME}     ${APIKEY}
    ${RES}=             Get Request     API  ?i=${IDFILME}&apikey=${APIKEY}
    Log                 ${RES.json()}
    Set Test Variable   ${RES}

Conferir o Status code
    [Arguments]     ${RESPONSE}    ${STATUSCODE_DESEJADO}
    Log                            ${RESPONSE.json()}
    Should Be Equal As Strings     ${RESPONSE.status_code}     ${STATUSCODE_DESEJADO}

Conferir se possui os campos - Filme Inexistente
    [Arguments]                         ${RESPONSE}                 
    &{DIC}  Convert To Dictionary       ${RESPONSE.json()}
    Dictionary Should Contain Key       ${DIC}        Response
    Dictionary Should Contain Key       ${DIC}        Error
    
Conferir se possui os campos - Caso Sucesso
    [Arguments]                         ${RESPONSE}        
    &{DIC}  Convert To Dictionary       ${RESPONSE.json()}
    Dictionary Should Contain Key       ${DIC}        Title
    Dictionary Should Contain Key       ${DIC}        Year          
    Dictionary Should Contain Key       ${DIC}        Language          
