*** Settings ***
Resource                    CommonsResource.robot
Documentation               Desafio 2 - Teste de API
Suite Setup                 Conectar API

*** Test Case ***

Validação do endpoint com os parametros especificos
    Requisitar URL                                  ${ID_FILME}     ${API_KEY}
    Conferir o Status code                          ${RES}          200
    Conferir se possui os campos - Caso Sucesso     ${RES}

Validação da Inexistencia de algum item
    Requisitar URL                                    ${ID_FILME_INEXISTENTE}     ${API_KEY}
    Conferir o Status code                            ${RES}          200
    Conferir se possui os campos - Filme Inexistente  ${RES}          